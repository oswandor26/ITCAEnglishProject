// importar express frameworkd 

const express = require('express') ; 
const app = express(); 

const cookieParser = require('cookie-parser'); 
const session = require('express-session'); 
const passport = require('passport');
const passportLocal = require('passport-local').Strategy; 



app.set("view engine" , 'ejs'); 
app.use(express.static('views')); 

// para leer los datos del formulario 
app.use(express.urlencoded({extended: true}))
app.use(cookieParser('mi key secress')) 
app.use(session({
    secret: 'mi key secress',
    resave : true , 
    saveUninitialized : true 
}))

//inicilizamos  passport para autenticacion 
app.use(passport.initialize());  
app.use(passport.session()); 




//configuracion de passport 
passport.use(new passportLocal(function(username ,password ,done ){

    if(username === "alumno" && password === "alumnoITCA"){
        return done(null, {id:1 , name:"Ronal"})
    }

    return done(null , false) 

}))

passport.serializeUser(function(user, done){

    done(null , user.id) 
}); 


passport.deserializeUser(function(id , done){

    done(null , {id:1 , name:"Ronal"})
})





// rutas  
app.get('/' , (req, res)=> {

    res.render("index") 

}); 


app.get('/login' , (req, res)=> {

    res.render("login"); 

}); 


//metodo post para el formulario 
app.post("/login" ,passport.authenticate('local' , {
    successRedirect:'/informacion', 
    failureRedirect : '/login'

})); 


var autenticado =(req, res , next)=>{

    if(req.user){
        return next()
    }else{
        res.json({"error":"error"})
    }
}

app.get('/informacion', autenticado , (req , res)=>{
    console.log(req.user) 
    res.render("informacion")
})


//rutas del menu 
app.get('/saludos1', (req , res)=>{
    res.render("./basico1/saludos1")
})


app.get('/verbotobe', (req , res)=>{
    res.render("./basico1/modalverbs")
})



app.get('/phrasalVerbs', (req , res)=>{
    res.render("./basico1/phrasalVerbs")
})






//ruta de avanzados 

app.get('/avanzadotema1', (req , res)=>{
    res.render("avanzadotema1")
})



app.get('/avanzadotema2', (req , res)=>{
    res.render("avanzadotema2")
})



app.get('/avanzadotema3', (req , res)=>{
    res.render("avanzadotema3")
})




app.get('/avanzadotema4', (req , res)=>{
    res.render("avanzadotema4")
})


//rutas  videos basicos 
app.get('/videobasico1', (req , res)=>{
    res.render("./basico1/videobasico1")
})


app.get('/videobasico2', (req , res)=>{
    res.render("./basico1/videobasico2")
})



app.get('/videobasico3', (req , res)=>{
    res.render("./basico1/videobasico3")
})


// videos de intermedio 

app.get('/videointermediotema1', (req , res)=>{
    res.render("./intermedio/videointermedio1")
})


app.get('/videointermediotema2', (req , res)=>{
    res.render("./intermedio/videointermedio2")
})


app.get('/videointermediotema3', (req , res)=>{
    res.render("./intermedio/videointermedio3")
})

app.get('/idioms', (req , res)=>{
    res.render("./intermedio/idioms")
})


// videos de avanzados  
app.get('/videoavanzadotema1', (req , res)=>{
    res.render("./avanzado/videoavanzado1")
})


app.get('/videoavanzadotema2', (req , res)=>{
    res.render("./avanzado/videoavanzado2")
})


app.get('/videoavanzadotema3', (req , res)=>{
    res.render("./avanzado/videoavanzado3")
})





app.listen(3000, () => { console.log("Server port 3000") }); 









